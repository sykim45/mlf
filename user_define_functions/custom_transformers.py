# this file only contains user-defined transformer(s)


from sklearn.feature_selection import SelectPercentile
from sklearn.feature_selection import f_classif


class WrapperSelectPercentile(SelectPercentile):
    def __init__(self, score_func=f_classif, percentile=30):
        self.score_func = score_func
        self.percentile = percentile
        super(WrapperSelectPercentile, self).__init__(score_func=self.score_func, percentile=self.percentile)

    def fit(self, x, y=None):
        super(WrapperSelectPercentile, self).fit(x, y)

    def transform(self, x, y=None):
        selected_features_idx = self.get_support()
        print('f_classif selected {}%, {} features.' .format(self.percentile, selected_features_idx.sum()))
        return super(WrapperSelectPercentile, self).transform(x)

    def fit_transform(self, x, y=None):
        return super(WrapperSelectPercentile, self).fit(x, y).transform(x)
