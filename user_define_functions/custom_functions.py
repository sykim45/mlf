# this file contains 'data_prepare' function, 'model_train' functions and 'model_select' function
#
# [important!] this file should contain TASK_INFO global variable (at the end of this file)
# includes 'data_prepare', 'model_train' and 'model_select'
# and some configuration variables
import contextlib
import sys
import time
from datetime import datetime
from importlib import import_module
from multiprocessing.pool import Pool

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, accuracy_score, f1_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier


from mlf.user_define_functions.custom_transformers import WrapperSelectPercentile

target_yrmn = 201705
INPUT_USER_CONFIG = {
    'tag_meta': {'tag_id': 'i9999',  # I-tag id에 대한 정책 미정
                 'tag_name': 'satisfaction_score',
                 'problem_type': 'binary_classification',
                 'selection_criterion': 'ROC_AUC',
                 },
    'dag_info': {'dag_purpose': 'train',
                 'schedule_interval': '@once',
                 'dag_args': {'dag_id': 'dag_mlf_v0.3_training',
                              'start_date': datetime.now(),
                              'owner': 'airflow',
                              'provide_context': True
                              }
                 },
    'notification_info': {'channel': 'SMS',
                          'recipients': 'Machine Learning Engineering Squad'
                          },
    'hive_info': {'hql': 'SELECT * FROM binary_train WHERE cuof_yrmn = ' + str(target_yrmn),
                  'user': '408074',
                  'host': '10.211.31.33', 'port': 10000,
                  'database': 'hcc_ml_score', 'auth_mechanism': 'PLAIN'}
}


def execute_hql_and_return_as_dataframe():
    hql = INPUT_USER_CONFIG['hive_info']['hql']
    hive_connection_params = {
        'user': INPUT_USER_CONFIG['hive_info']['user'],
        'host': INPUT_USER_CONFIG['hive_info']['host'],
        'port': INPUT_USER_CONFIG['hive_info']['port'],
        'auth_mechanism': INPUT_USER_CONFIG['hive_info']['auth_mechanism'],
        'database': INPUT_USER_CONFIG['hive_info']['database']
    }

    n_jobs = 1

    connection = import_module('impala.dbapi').connect(**hive_connection_params)
    cursor = connection.cursor()
    cursor.execute(hql)

    if cursor.description is None:
        # return None if the hql does not yield results
        df = None
    else:
        # needs to be optimized (as_pandas is too slow)
        if n_jobs == 1:
            df = import_module('impala.util').as_pandas(cursor)
        else:
            # support multiprocessing
            df = as_pandas(cursor)

        # remove database(scheme) name from column names
        df.columns = list(map(lambda x: x if '.' not in x else x.split('.')[-1], df.columns))

    cursor.close()
    connection.close()

    return df


# support multiprocessing
def as_pandas(self, cursor):
    names = [metadata[0] for metadata in cursor.description]
    records = cursor.fetchall()
    num_rows = len(records)
    num_rows_of_split = int(num_rows / self.n_jobs)

    records_splits = []
    for i in range(self.n_jobs):
        start = i * num_rows_of_split
        end = num_rows if i == self.n_jobs - 1 \
            else (i + 1) * num_rows_of_split
        object_for_pass = (records[start:end], names)  # records and names
        records_splits.append(object_for_pass)

    pool = Pool(self.n_jobs)
    df = pd.concat(
        pool.map(self._dataframe_from_records, records_splits), axis=0)
    df.reset_index(drop=True, inplace=True)  # for exactly same result

    return df


def dataframe_from_records(records_and_names):
    return pd.DataFrame.from_records(records_and_names[0], columns=records_and_names[1])


def prepare_data(df):
    response = 'target'
    df.set_index(['csno'], inplace=True)

    # use only data of 201703 for fast modeling
    # df = df[df['cuof_yrmn'] == 201703]

    df_X = df.drop(['cuof_yrmn', response], axis=1)
    df_y = df[response].copy()
    df_y = df_y.values.ravel()
    df_X.replace(-999999, 2, inplace=True)  # Replace -999999 in var3 column with most common value 2

    return df_X, df_y


# area under the precision recall curve
def pr_auc_score(y_true, y_score, pos_label=None, sample_weight=None):
    from sklearn.metrics import precision_recall_curve, auc
    precision, recall, thresholds = \
        precision_recall_curve(y_true, y_score, pos_label, sample_weight)
    return auc(recall, precision)


# model trainer 1
def modeling_pipeline_1(df_X, df_y):
    random_state = 10

    # 1. split data
    X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size=0.20, random_state=random_state)
    print('X_train shape:', X_train.shape)
    print('X_test shape:', X_test.shape)

    # 2. Feature Selection & classify using sklearn.pipeline
    pipeline = Pipeline([
        ('feature_selector_by_f_classif', WrapperSelectPercentile()),
        ('feature_selector_by_dt', SelectFromModel(DecisionTreeClassifier(random_state=random_state),
                                                   prefit=False)),
        ('classification', RandomForestClassifier(random_state=random_state))
    ])

    parameters_distribs = {
        'feature_selector_by_f_classif__percentile': (30, 80),
        'feature_selector_by_dt__estimator__max_depth': (3, 8),
        'classification__n_estimators': (30, 70)
    }

    # GridSearchCV
    rnd_search = GridSearchCV(pipeline, param_grid=parameters_distribs, cv=3, scoring='roc_auc')

    start_time = time.perf_counter()
    rnd_search.fit(X_train, y_train)
    elapsed_time = time.perf_counter() - start_time
    print('GridSearchCV taked time : ' + str(elapsed_time) + 'secs')

    hyperparameters = rnd_search.best_params_
    print('best_parameters: ', hyperparameters)
    best_model = rnd_search.best_estimator_
    predicted_values = best_model.predict_proba(X_test)[:, 1]
    auc = roc_auc_score(y_test, predicted_values)  # AUROC
    print("ROC_AUC: ", auc)
    pr_auc = pr_auc_score(y_test, predicted_values)  # AUPRC
    print("PR_AUC: ", pr_auc)

    predicted_labels = best_model.predict(X_test)
    accuracy = accuracy_score(y_test, predicted_labels)
    print("ACCURACY: ", accuracy)
    f1 = f1_score(y_test, predicted_labels)
    print("f1_score: ", f1)

    df_total_X = np.r_[X_train, X_test]
    df_total_y = np.r_[y_train, y_test]

    start_time = time.perf_counter()
    best_model.fit(df_total_X, df_total_y)
    elapsed_time = time.perf_counter() - start_time
    print('Final Model fitting time : ' + str(elapsed_time) + 'secs')

    feature_importances = best_model.named_steps['classification'].feature_importances_

    # selected features by WrapperSelectPercentile()
    selected_by_f_classif_idx = best_model.named_steps['feature_selector_by_f_classif'].get_support()
    selected_features_by_f_classif = [f for i, f in enumerate(df_X.columns.tolist()) if selected_by_f_classif_idx[i]]

    # selected features by SelectFromModel
    selected_by_dt_idx = best_model.named_steps['feature_selector_by_dt'].get_support()
    selected_features_by_dt = [f for i, f in enumerate(selected_features_by_f_classif) if selected_by_dt_idx[i]]

    model_info = {
        'model_info': best_model.steps,
        'hyperparameters': hyperparameters,
        'metric': {
            'ROC_AUC': auc,
            'PR_AUC': pr_auc,
            'ACCURACY': accuracy,
            'f1_score': f1
        },
        'interpretation': {
            'feature_importances': dict(zip(selected_features_by_dt, feature_importances))
        }
    }

    with printoptions(threshold=sys.maxsize):
        print(best_model)
        print(model_info)

    return best_model, model_info


# model trainer 2 (copy & paste, but a little bit different)
def modeling_pipeline_2(df_X, df_y):
    random_state = 10

    # 1. split data
    X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size=0.20, random_state=random_state)
    print('X_train shape:', X_train.shape)
    print('X_test shape:', X_test.shape)

    # 2. Feature Selection & classify using sklearn.pipeline
    pipeline = Pipeline([
        ('feature_selector_by_f_classif', WrapperSelectPercentile()),
        ('feature_selector_by_lsvc', SelectFromModel(LinearSVC(), prefit=False)),
        ('classification', GradientBoostingClassifier(random_state=random_state))
    ])

    parameters_distribs = {
        'feature_selector_by_f_classif__percentile': (50, 80),
        'feature_selector_by_lsvc__estimator__C': (0.5, 1.0),
        'classification__n_estimators': (50, 100)
    }

    # GridSearchCV
    rnd_search = GridSearchCV(pipeline, param_grid=parameters_distribs, cv=3, scoring='roc_auc')

    start_time = time.perf_counter()
    rnd_search.fit(X_train, y_train)
    elapsed_time = time.perf_counter() - start_time
    print('GridSearchCV taked time : ' + str(elapsed_time) + 'secs')

    hyperparameters = rnd_search.best_params_
    print('best_parameters: ', hyperparameters)
    best_model = rnd_search.best_estimator_
    predicted_values = best_model.predict_proba(X_test)[:, 1]
    auc = roc_auc_score(y_test, predicted_values)  # AUROC
    print("ROC_AUC: ", auc)
    pr_auc = pr_auc_score(y_test, predicted_values)  # AUPRC
    print("PR_AUC: ", pr_auc)

    predicted_labels = best_model.predict(X_test)
    accuracy = accuracy_score(y_test, predicted_labels)
    print("ACCURACY: ", accuracy)
    f1 = f1_score(y_test, predicted_labels)
    print("f1_score: ", f1)

    df_total_X = np.r_[X_train, X_test]
    df_total_y = np.r_[y_train, y_test]

    start_time = time.perf_counter()
    best_model.fit(df_total_X, df_total_y)
    elapsed_time = time.perf_counter() - start_time
    print('Final Model fitting time : ' + str(elapsed_time) + 'secs')

    feature_importances = best_model.named_steps['classification'].feature_importances_

    # selected features by WrapperSelectPercentile()
    selected_by_f_classif_idx = best_model.named_steps['feature_selector_by_f_classif'].get_support()
    selected_features_by_f_classif = [f for i, f in enumerate(df_X.columns.tolist()) if selected_by_f_classif_idx[i]]

    # selected features by SelectFromModel
    selected_by_lsvc_idx = best_model.named_steps['feature_selector_by_lsvc'].get_support()
    selected_features_by_lsvc = [f for i, f in enumerate(selected_features_by_f_classif) if selected_by_lsvc_idx[i]]

    model_info = {
        'model_info': best_model.steps,
        'hyperparameters': hyperparameters,
        'metric': {
            'ROC_AUC': auc,
            'PR_AUC': pr_auc,
            'ACCURACY': accuracy,
            'f1_score': f1
        },
        'interpretation': {
            'feature_importances': dict(zip(selected_features_by_lsvc, feature_importances))
        }
    }

    with printoptions(threshold=sys.maxsize):
        print(best_model)
        print(model_info)

    return best_model, model_info


# model trainer 3
def modeling_pipeline_3(df_X, df_y):
    random_state = 10

    # 1. split data
    X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size=0.20, random_state=random_state)
    print('X_train shape:', X_train.shape)
    print('X_test shape:', X_test.shape)

    # 2. Feature Selection & classify using sklearn.pipeline
    pipeline = Pipeline([
        ('feature_selector_by_f_classif', WrapperSelectPercentile()),
        ('feature_selector_by_lr', SelectFromModel(LogisticRegression(random_state=random_state),
                                                   prefit=False)),
        ('classification', ExtraTreesClassifier(random_state=random_state))
    ])

    parameters_distribs = {
        'feature_selector_by_f_classif__percentile': (30, 80),
        'feature_selector_by_lr__estimator__C': (0.5, 1.0),
        'classification__n_estimators': (10, 30)
    }

    # GridSearchCV
    rnd_search = GridSearchCV(pipeline, param_grid=parameters_distribs, cv=3, scoring='roc_auc')

    start_time = time.perf_counter()
    rnd_search.fit(X_train, y_train)
    elapsed_time = time.perf_counter() - start_time
    print('GridSearchCV taked time : ' + str(elapsed_time) + 'secs')

    hyperparameters = rnd_search.best_params_
    print('best_parameters: ', hyperparameters)
    best_model = rnd_search.best_estimator_
    predicted_values = best_model.predict_proba(X_test)[:, 1]
    auc = roc_auc_score(y_test, predicted_values)  # AUROC
    print("ROC_AUC: ", auc)
    pr_auc = pr_auc_score(y_test, predicted_values)  # AUPRC
    print("PR_AUC: ", pr_auc)

    predicted_labels = best_model.predict(X_test)
    accuracy = accuracy_score(y_test, predicted_labels)
    print("ACCURACY: ", accuracy)
    f1 = f1_score(y_test, predicted_labels)
    print("f1_score: ", f1)

    df_total_X = np.r_[X_train, X_test]
    df_total_y = np.r_[y_train, y_test]

    start_time = time.perf_counter()
    best_model.fit(df_total_X, df_total_y)
    elapsed_time = time.perf_counter() - start_time
    print('Final Model fitting time : ' + str(elapsed_time) + 'secs')

    feature_importances = best_model.named_steps['classification'].feature_importances_

    # selected features by WrapperSelectPercentile()
    selected_by_f_classif_idx = best_model.named_steps['feature_selector_by_f_classif'].get_support()
    selected_features_by_f_classif = [f for i, f in enumerate(df_X.columns.tolist()) if selected_by_f_classif_idx[i]]

    # selected features by SelectFromModel
    selected_by_lr_idx = best_model.named_steps['feature_selector_by_lr'].get_support()
    selected_features_by_lr = [f for i, f in enumerate(selected_features_by_f_classif) if selected_by_lr_idx[i]]

    model_info = {
        'model_info': best_model.steps,
        'hyperparameters': hyperparameters,
        'metric': {
            'ROC_AUC': auc,
            'PR_AUC': pr_auc,
            'ACCURACY': accuracy,
            'f1_score': f1
        },
        'interpretation': {
            'feature_importances': dict(zip(selected_features_by_lr, feature_importances))
        }
    }

    with printoptions(threshold=sys.maxsize):
        print(best_model)
        print(model_info)

    return best_model, model_info


def select_model(best_model_list, model_info_list):
    max_val = -99999999
    max_idx = 0

    for idx, val in enumerate(model_info_list):
        metric_val = val['metric']['ROC_AUC']
        if metric_val > max_val:
            max_val = metric_val
            max_idx = idx

    print('The selected model index: {}'.format(max_idx))

    print('The selected model is:')
    with printoptions(threshold=sys.maxsize):
        print(best_model_list[max_idx])


@contextlib.contextmanager
def printoptions(*args, **kwargs):
    original = np.get_printoptions()
    np.set_printoptions(*args, **kwargs)
    try:
        yield
    finally:
        np.set_printoptions(**original)
