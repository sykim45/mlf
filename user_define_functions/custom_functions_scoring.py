from multiprocessing.pool import Pool
from importlib import import_module
from datetime import datetime
import pandas as pd
import numpy as np

target_yrmn = 201705
INPUT_USER_CONFIG = {
    'tag_meta': {'tag_id': 'i9999',  # I-tag id에 대한 정책 미정
                 'tag_name': 'satisfaction_score',
                 'problem_type': 'binary_classification',
                 'selection_criterion': 'ROC_AUC',
                 },
    'dag_info': {'dag_purpose': 'score',
                 'schedule_interval': '@once',
                 'dag_args': {'dag_id': 'dag_mlf_v0.3_scoring',
                              'start_date': datetime.now(),
                              'owner': 'airflow',
                              'provide_context': True
                              }
                 },
    'notification_info': {'channel': 'SMS',
                          'recipients': 'Machine Learning Engineering Squad'
                          },
    'hive_info': {'hql': 'SELECT * FROM binary_test WHERE cuof_yrmn = ' + str(target_yrmn),
                  'user': '408074',
                  'host': '10.211.31.33', 'port': 10000,
                  'database': 'hcc_ml_score', 'auth_mechanism': 'PLAIN'}
}


def execute_hql_and_return_as_dataframe():
    hql = INPUT_USER_CONFIG['hive_info']['hql']
    hive_connection_params = {
        'user': INPUT_USER_CONFIG['hive_info']['user'],
        'host': INPUT_USER_CONFIG['hive_info']['host'],
        'port': INPUT_USER_CONFIG['hive_info']['port'],
        'auth_mechanism': INPUT_USER_CONFIG['hive_info']['auth_mechanism'],
        'database': INPUT_USER_CONFIG['hive_info']['database']
    }

    n_jobs = 1

    connection = import_module('impala.dbapi').connect(**hive_connection_params)
    cursor = connection.cursor()
    cursor.execute(hql)

    if cursor.description is None:
        # return None if the hql does not yield results
        df = None
    else:
        # needs to be optimized (as_pandas is too slow)
        if n_jobs == 1:
            df = import_module('impala.util').as_pandas(cursor)
        else:
            # support multiprocessing
            df = as_pandas(cursor)

        # remove database(scheme) name from column names
        df.columns = list(map(lambda x: x if '.' not in x else x.split('.')[-1], df.columns))

    cursor.close()
    connection.close()

    return df


# support multiprocessing
def as_pandas(cursor):
    n_jobs = 1
    names = [metadata[0] for metadata in cursor.description]
    records = cursor.fetchall()
    num_rows = len(records)
    num_rows_of_split = int(num_rows / n_jobs)

    records_splits = []
    for i in range(n_jobs):
        start = i * num_rows_of_split
        end = num_rows if i == n_jobs - 1 \
            else (i + 1) * num_rows_of_split
        object_for_pass = (records[start:end], names)  # records and names
        records_splits.append(object_for_pass)

    pool = Pool(n_jobs)
    df = pd.concat(
        pool.map(dataframe_from_records, records_splits), axis=0)
    df.reset_index(drop=True, inplace=True)  # for exactly same result

    return df


def dataframe_from_records(records_and_names):
    return pd.DataFrame.from_records(records_and_names[0], columns=records_and_names[1])


def prepare_data_for_scoring(df):
    df.set_index(['csno'], inplace=True)
    df.replace(-999999, 2, inplace=True)  # Replace -999999 in var3 column with most common value 2
    df = df.drop(['cuof_yrmn'], axis=1)  # essential!
    print('original scoring_set shape:', df.shape)
    return df


# predict and print scores
def func_score(model, df):
    predicted_score = model.predict_proba(df)[:, 1]
    score_df = pd.DataFrame(data=predicted_score, index=df.index,
                            columns=[INPUT_USER_CONFIG['tag_meta']['tag_id']])
    score_df.insert(loc=0, column='cuof_yrmn', value=str(target_yrmn))
    print('Score output (head):\n{}'.format(score_df.head()))

    # restore csno
    score_df = score_df.reset_index()

    return score_df


def insert_with_impyla(df):
    insert_params = {
        'name': 'i9999_demo_score',
        'overwrite': False,
        'comment': None,
        'row_format_statement': None,
        'stored_as_statement': None,
        'partition_columns': None,
        'hdfs_path': None,
        'limit_of_insert_query_length': 100000
    }

    hive_connection_params = {
        'user': INPUT_USER_CONFIG['hive_info']['user'],
        'host': INPUT_USER_CONFIG['hive_info']['host'],
        'port': INPUT_USER_CONFIG['hive_info']['port'],
        'auth_mechanism': INPUT_USER_CONFIG['hive_info']['auth_mechanism'],
        'database': INPUT_USER_CONFIG['hive_info']['database']
    }
    df.reset_index(drop=True, inplace=True)

    table_name = insert_params['name']

    connection = import_module('impala.dbapi').connect(**hive_connection_params)
    cursor = connection.cursor()

    # use database
    if hive_connection_params['database'] is not None:
        cursor.execute('use {}'.format(hive_connection_params['database']))

    # drop table when overwrite=True
    if insert_params['overwrite']:
        cursor.execute('drop table if exists ' + table_name)

    # create table if not exists
    create_table_statement = get_create_table_statement(df, insert_params)
    cursor.execute(create_table_statement)

    # insert lines by using 'insert into' queries (line by line)
    # _insert_with_executemany() is guaranteed by impyla but extremely slow
    insert_with_execute(df, table_name, connection, cursor, insert_params['limit_of_insert_query_length'])

    cursor.close()
    connection.close()


def get_create_table_statement(df, insert_params):
    name = insert_params['name']
    comment = insert_params['comment']
    row_format = insert_params['row_format_statement']
    stored_as = insert_params['stored_as_statement']
    partition_cols = insert_params['partition_columns']
    hdfs_path = insert_params['hdfs_path']

    types_series = df.dtypes.apply(get_type_as_string)
    if partition_cols is None:
        partition_cols = []
    cond_partition = types_series.index.isin(partition_cols)
    cond_not_partition = ~cond_partition

    # create table [name] ( columns_with_types )
    columns = types_series[cond_not_partition].index
    column_types = types_series[cond_not_partition]
    columns_with_types = ', '.join(map(lambda x: x[0] + ' ' + x[1],
                                       zip(columns, column_types)))

    # create table [name] ( columns_with_types )
    # partitioned by ( partition_columns_with_types )
    columns = types_series[cond_partition].index
    column_types = types_series[cond_partition]
    partition_columns_with_types = ', '.join(map(
        lambda x: x[0] + ' ' + x[1], zip(columns, column_types)))

    statement_create_table = 'create table if not exists ' + name + \
                             ' (' + columns_with_types + ')'

    if comment is not None:
        statement_create_table += ' comment \'' + comment + '\''
    if len(partition_cols) > 0:
        statement_create_table += \
            ' partitioned by (' + partition_columns_with_types + ')'
    if row_format is not None:
        statement_create_table += ' ' + row_format
    if stored_as is not None:
        statement_create_table += ' ' + stored_as
    if hdfs_path is not None:
        statement_create_table += ' location \'' + hdfs_path + '\''
        statement_create_table = statement_create_table.replace(
            'create table', 'create external table')

    return statement_create_table


# run multiple queries for inserting entire data into the table
def insert_with_execute(df, table_name, connection, cursor,
                        limit_of_insert_query_length):
    limit_chunk_len = limit_of_insert_query_length
    ql = 'insert into %s (%s) values ' % (
        table_name, ', '.join(df.columns))
    ql_len = len(ql)

    types_series = df.dtypes.apply(get_type_as_string)
    string_indices = np.where(types_series == 'string')[0]
    int_indices = np.where(types_series == 'int')[0]
    data = df.apply(
        lambda x: row_to_str(x, string_indices, int_indices), axis=1)

    data_lengths = data.apply(len) + 2  # +2: for ', ' between rows
    limit_idx = len(data) - 1

    chunk_len = ql_len
    indices_for_combining = []

    for idx, line in data.iteritems():
        indices_for_combining.append(idx)
        chunk_len += data_lengths[idx]

        if chunk_len >= limit_chunk_len or idx == limit_idx:
            ql_combined = ql + ', '.join(data[indices_for_combining])
            cursor.execute(ql_combined)
            chunk_len = ql_len
            indices_for_combining = []

    connection.commit()


def get_type_as_string(dtype):
    if np.issubdtype(dtype, np.number):
        if np.issubdtype(dtype, np.integer):
            return 'int'
        else:
            return 'double'
    else:
        return 'string'


def row_to_str(x, string_indices, int_indices):
    x_str = x.astype(str)

    if len(string_indices):
        x_str[string_indices] = x_str[string_indices].apply(repr)

    # prevent '.0' in the final result
    if len(int_indices):
        x_str[int_indices] = x_str[int_indices].apply(
            lambda s: s.split('.')[0])

    return '(%s)' % ', '.join(x_str)
