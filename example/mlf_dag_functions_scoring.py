from mlf.mlf_utils import *
from mlf.mlf_file_utils import *
from mlf.user_define_functions.custom_functions_scoring import *


def load_config(**kwargs):
    """
    사용자가 입력한 config 값과 DAG 실행 정보를 ModelDB에 저장한다.
    """
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        insert_inferred_tag_meta(INPUT_USER_CONFIG)
        insert_inferred_tag_dag_info(INPUT_USER_CONFIG)
        insert_tag_dag_run_state(dag_run, INPUT_USER_CONFIG)
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)


def load_data_from_hive(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        # User Define Function
        df = execute_hql_and_return_as_dataframe()

        pkl_file_name = make_pkl_file_name('origin', task_instance.dag_id, dag_run.run_id)
        pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', pkl_file_name)

        dump_pickle(df, pkl_file_path)

        insert_source_dataset(dag_run, task_instance, pkl_file_path)
        return pkl_file_path
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)


def prepare_origin_data(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        origin_data_path = task_instance.xcom_pull(key=None, task_ids='load_data_from_hive')
        origin_df = load_pickle(origin_data_path)
        # User Define Function
        df = prepare_data_for_scoring(origin_df)

        df_pkl_file_name = make_pkl_file_name("df", task_instance.dag_id, dag_run.run_id)
        df_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', df_pkl_file_name)

        dump_pickle(df, df_pkl_file_path)

        tag_id = INPUT_USER_CONFIG['tag_meta']['tag_id']

        selected_model_file_path = get_selected_model_file_path(dag_run, tag_id)

        source_dataset_id = get_source_dataset_id(dag_run, task_instance)
        insert_wrangled_dataset(dag_run, task_instance, df_pkl_file_path, source_dataset_id)

        prepare_score_data_path = {'df_pkl_file_path': df_pkl_file_path,
                                   'selected_model_file_path': selected_model_file_path
                                   }
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)
    return prepare_score_data_path


def scoring(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']

    try:
        prepare_data_set = task_instance.xcom_pull(task_ids='prepare_data')

        model = load_pickle(prepare_data_set['selected_model_file_path'])
        df = load_pickle(prepare_data_set['df_pkl_file_path'])

        score_df = func_score(model, df)

        score_df_pkl_file_name = make_pkl_file_name("score", task_instance.dag_id, dag_run.run_id)
        score_df_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', score_df_pkl_file_name)

        dump_pickle(score_df, score_df_pkl_file_path)

        wrangled_dataset_id = get_wrangled_dataset_id(dag_run, task_instance)
        insert_scoring(dag_run, task_instance, wrangled_dataset_id)

    except Exception as e:
        insert_error_log(dag_run, task_instance, e)

    return score_df_pkl_file_path


def save_score_to_hive(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        score_df_path = task_instance.xcom_pull(task_ids='scoring')
        score_df = load_pickle(score_df_path)
        insert_with_impyla(score_df)

    except Exception as e:
        insert_error_log(dag_run, task_instance, e)


def notify(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    error_list = []
    notification_info = INPUT_USER_CONFIG['notification_info']
    try:
        if not error_log_exists(dag_run):
            insert_success_state(dag_run, notification_info)
        else:
            error_logs = get_error_logs(dag_run)
            make_error_message(error_list, error_logs)
            insert_failed_state(dag_run, error_list, notification_info)

    except Exception as e:
        insert_error_log(dag_run, task_instance, e)