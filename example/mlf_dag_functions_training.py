from mlf.mlf_utils import *
from mlf.mlf_file_utils import *
from mlf.user_define_functions.custom_functions import *


def load_config(**kwargs):
    """
    사용자가 입력한 config 값과 DAG 실행 정보를 ModelDB에 저장한다.
    """
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        insert_inferred_tag_meta(INPUT_USER_CONFIG)
        insert_inferred_tag_dag_info(INPUT_USER_CONFIG)
        insert_tag_dag_run_state(dag_run, INPUT_USER_CONFIG)
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)


def load_data_from_hive(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        # User Define Function
        df = execute_hql_and_return_as_dataframe()

        pkl_file_name = make_pkl_file_name('origin', task_instance.dag_id, dag_run.run_id)
        pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', pkl_file_name)

        dump_pickle(df, pkl_file_path)

        insert_source_dataset(dag_run, task_instance, pkl_file_path)
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)
    return pkl_file_path


def prepare_origin_data(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        origin_data_path = task_instance.xcom_pull(key=None, task_ids='load_data_from_hive')
        df = load_pickle(origin_data_path)
        # User Define Function
        df_X, df_y = prepare_data(df)

        df_x_pkl_file_name = make_pkl_file_name("df_X", task_instance.dag_id, dag_run.run_id)
        df_y_pkl_file_name = make_pkl_file_name("df_y", task_instance.dag_id, dag_run.run_id)
        df_x_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', df_x_pkl_file_name)
        df_y_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', df_y_pkl_file_name)

        prepare_dataset_path = [df_x_pkl_file_path, df_y_pkl_file_path]

        dump_pickle(df_X, df_x_pkl_file_path)
        dump_pickle(df_y, df_y_pkl_file_path)

        source_dataset_id = get_source_dataset_id(dag_run, task_instance)
        insert_wrangled_dataset(dag_run, task_instance, prepare_dataset_path, source_dataset_id)

        prepare_data_path = {'df_x_pkl_file_path': df_x_pkl_file_path,
                             'df_y_pkl_file_path': df_y_pkl_file_path
                             }
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)
    return prepare_data_path


def modeling(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        prepare_data_set = task_instance.xcom_pull(task_ids='prepare_data')

        df_X = load_pickle(prepare_data_set['df_x_pkl_file_path'])
        df_y = load_pickle(prepare_data_set['df_y_pkl_file_path'])

        # User Define Function
        model, model_info = modeling_pipeline_1(df_X, df_y)

        pipeline_pkl_file_name = make_pkl_file_name("pipeline_1_", task_instance.dag_id, dag_run.run_id)
        model_info_pkl_file_name = make_pkl_file_name("pipeline_1_model_info_", task_instance.dag_id, dag_run.run_id)

        pipeline_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', pipeline_pkl_file_name)
        model_info_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle',
                                                      model_info_pkl_file_name)

        dump_pickle(model, pipeline_pkl_file_path)
        dump_pickle(model_info, model_info_pkl_file_path)

        wrangled_dataset_id = get_wrangled_dataset_id(dag_run, task_instance)
        insert_predictive_model(dag_run, task_instance, model_info['hyperparameters'], str(model_info['model_info']),
                                pipeline_pkl_file_path, wrangled_dataset_id)

        predictive_model_id = get_predictive_model_id(dag_run, task_instance)
        insert_model_evaluation(model_info['metric'], predictive_model_id)
        insert_model_feature_importance(model_info['interpretation'], predictive_model_id)

        modeling_data_path = {'model': model,
                              'model_info': model_info
                              }
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)
    return modeling_data_path


def modeling2(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        prepare_data_set = task_instance.xcom_pull(task_ids='prepare_data')

        # df_X = load_pickle(prepare_data_set['df_x_pkl_file_path'])
        df_X = load_pickle(prepare_data_set['df_x_pkl_file_pat'])
        df_y = load_pickle(prepare_data_set['df_y_pkl_file_path'])

        # User Define Function
        model, model_info = modeling_pipeline_2(df_X, df_y)

        pipeline_pkl_file_name = make_pkl_file_name("pipeline_2_", task_instance.dag_id, dag_run.run_id)
        model_info_pkl_file_name = make_pkl_file_name("pipeline_2_model_info_", task_instance.dag_id, dag_run.run_id)
        pipeline_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', pipeline_pkl_file_name)
        model_info_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle',
                                                      model_info_pkl_file_name)

        dump_pickle(model, pipeline_pkl_file_path)
        dump_pickle(model_info, model_info_pkl_file_path)

        wrangled_dataset_id = get_wrangled_dataset_id(dag_run, task_instance)
        insert_predictive_model(dag_run,
                                task_instance,
                                model_info['hyperparameters'],
                                str(model_info['model_info']),
                                pipeline_pkl_file_path,
                                wrangled_dataset_id
                                )

        predictive_model_id = get_predictive_model_id(dag_run, task_instance)
        insert_model_evaluation(model_info['metric'], predictive_model_id)
        insert_model_feature_importance(model_info['interpretation'], predictive_model_id)

        modeling_data_path = {'model': model,
                              'model_info': model_info
                              }
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)
    return modeling_data_path


def modeling3(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    try:
        prepare_data_set = task_instance.xcom_pull(task_ids='prepare_data')

        df_X = load_pickle(prepare_data_set['df_x_pkl_file_path'])
        df_y = load_pickle(prepare_data_set['df_y_pkl_file_path'])

        # User Define Function
        model, model_info = modeling_pipeline_3(df_X, df_y)

        pipeline_pkl_file_name = make_pkl_file_name("pipeline_1_", task_instance.dag_id, dag_run.run_id)
        model_info_pkl_file_name = make_pkl_file_name("pipeline_1_model_info_", task_instance.dag_id, dag_run.run_id)

        pipeline_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle', pipeline_pkl_file_name)
        model_info_pkl_file_path = make_pkl_file_path('/home/ptagadm/dags/mlf_refactoring/pickle',
                                                      model_info_pkl_file_name)

        dump_pickle(model, pipeline_pkl_file_path)
        dump_pickle(model_info, model_info_pkl_file_path)

        wrangled_dataset_id = get_wrangled_dataset_id(dag_run, task_instance)
        insert_predictive_model(dag_run, task_instance, model_info['hyperparameters'], str(model_info['model_info']),
                                pipeline_pkl_file_path, wrangled_dataset_id)

        predictive_model_id = get_predictive_model_id(dag_run, task_instance)
        insert_model_evaluation(model_info['metric'], predictive_model_id)
        insert_model_feature_importance(model_info['interpretation'], predictive_model_id)

        modeling_data_path = {'model': model,
                              'model_info': model_info
                              }
    except Exception as e:
        insert_error_log(dag_run, task_instance, e)
    return modeling_data_path


def select_best_model(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    model_list = []
    model_info_list = []
    try:
        model_data_set = task_instance.xcom_pull(task_ids='modeling')
        model2_data_set = task_instance.xcom_pull(task_ids='modeling2')
        model3_data_set = task_instance.xcom_pull(task_ids='modeling3')

        if model_data_set is not None:
            model_list.append(model_data_set['model'])
            model_info_list.append(model_data_set['model_info'])
        if model2_data_set is not None:
            model_list.append(model2_data_set['model'])
            model_info_list.append(model2_data_set['model_info'])
        if model3_data_set is not None:
            model_list.append(model3_data_set['model'])
            model_info_list.append(model3_data_set['model_info'])

        # User Define Function
        select_model(model_list, model_info_list)

        selection_criterion = get_tag_id(task_instance)
        selected_model_id = get_selected_model_id(dag_run, task_instance, selection_criterion)

        insert_selected_model(dag_run, task_instance, selected_model_id)

    except Exception as e:
        insert_error_log(dag_run, task_instance, e)


def notify(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']
    error_list = []
    notification_info = INPUT_USER_CONFIG['notification_info']
    try:
        if not error_log_exists(dag_run):
            insert_success_state(dag_run, notification_info)
        else:
            error_logs = get_error_logs(dag_run)
            make_error_message(error_list, error_logs)
            insert_failed_state(dag_run, error_list, notification_info)

    except Exception as e:
        insert_error_log(dag_run, task_instance, e)