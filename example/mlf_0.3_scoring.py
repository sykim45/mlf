from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from mlf.example.mlf_dag_functions_scoring import *

from mlf.user_define_functions.custom_functions_scoring import *

args = INPUT_USER_CONFIG['dag_info']['dag_args']

dag = DAG(
    dag_id=INPUT_USER_CONFIG['dag_info']['dag_args']['dag_id'],
    schedule_interval = INPUT_USER_CONFIG['dag_info']['schedule_interval'],
    default_args=args
)


node_load_config = PythonOperator(
    task_id='load_config',
    python_callable=load_config,
    dag=dag
)

node_load_data_from_hive = PythonOperator(
    task_id='load_data_from_hive',
    python_callable=load_data_from_hive,
    dag=dag
)

node_prepare_data = PythonOperator(
    task_id='prepare_data',
    python_callable=prepare_origin_data,
    dag=dag
)

node_scoring = PythonOperator(
    task_id='scoring',
    python_callable=scoring,
    dag=dag
)

node_save_score_to_hive = PythonOperator(
    task_id='save_score_to_hive',
    python_callable=save_score_to_hive,
    dag=dag
)

node_notify = PythonOperator(
    task_id='notify',
    python_callable=notify,
    trigger_rule="all_done",
    dag=dag
)

node_load_config >> node_load_data_from_hive >> node_prepare_data
node_prepare_data >> node_scoring >> node_save_score_to_hive >> node_notify