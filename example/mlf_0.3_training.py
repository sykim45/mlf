from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

from mlf.example.mlf_dag_functions_training import *

args = INPUT_USER_CONFIG['dag_info']['dag_args']

dag = DAG(
    dag_id=INPUT_USER_CONFIG['dag_info']['dag_args']['dag_id'],
    schedule_interval = INPUT_USER_CONFIG['dag_info']['schedule_interval'],
    default_args=args
)

node_load_config = PythonOperator(
    task_id='load_config',
    python_callable=load_config,
    dag=dag
)

node_load_data_from_hive = PythonOperator(
    task_id='load_data_from_hive',
    python_callable=load_data_from_hive,
    dag=dag
)

node_prepare_data = PythonOperator(
    task_id='prepare_data',
    python_callable=prepare_origin_data,
    dag=dag
)

node_modeling = PythonOperator(
    task_id='modeling',
    python_callable=modeling,
    dag=dag
)

node_modeling2 = PythonOperator(
    task_id='modeling2',
    python_callable=modeling2,
    dag=dag
)

node_modeling3 = PythonOperator(
    task_id='modeling3',
    python_callable=modeling3,
    dag=dag
)

node_select_model = PythonOperator(
    task_id='select_model',
    python_callable=select_best_model,
    trigger_rule="all_done",
    dag=dag
)

node_notify = PythonOperator(
    task_id='notify',
    python_callable=notify,
    trigger_rule="all_done",
    dag=dag
)

node_load_config >> node_load_data_from_hive >> node_prepare_data
node_prepare_data >> node_modeling >> node_select_model
node_prepare_data >> node_modeling2 >> node_select_model
node_prepare_data >> node_modeling3 >> node_select_model
node_select_model >> node_notify
